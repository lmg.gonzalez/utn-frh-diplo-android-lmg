fun main() {

    var sumaTotal: Int? = suma1()
    println("La suma total es = $sumaTotal")

}

fun suma1(): Int? {

    var i: Int = 0
    var sumaParcial: Int? = 0
    var valor: String?

   while (i < 5) {

       print("Ingrese un numero: ")
       valor = readLine()
       var n: Int? = valor?.toInt()
       if (n != null && sumaParcial != null){
           sumaParcial += n
       }

       i ++
   }

    return sumaParcial
}