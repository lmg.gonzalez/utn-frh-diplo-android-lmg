fun main() {

    var suma_total: Int? = suma3()
    println("La suma total es = $suma_total")
}

fun suma3(): Int? {

    var sumaParcial: Int = 0

    var i: Int = 1
    var valor: Int? = 0
    var sumador: Int = 0

    while (i != 0) {
        try {
            print("Ingrese un numero: ")
            valor = readLine()?.toInt()!!
            sumaParcial += valor
        } catch (e: NumberFormatException) {
            println("Esperaba un entero, me diste otra cosa")
            i = 0 //sale del while
        } catch (e: NullPointerException) {
            println("Esperaba un entero, me diste null, ni siquiera un String")
            i = 0 //sale del while
        }
    }

    return sumaParcial
}