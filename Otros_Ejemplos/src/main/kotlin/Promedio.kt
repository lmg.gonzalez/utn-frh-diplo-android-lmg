fun main() {

    var promedio: Double? = promedio()
    println("El promedio total es = $promedio")
}

fun promedio(): Double {

    var sumaParcial: Double = 0.0

    var promedio_total: Double = 0.0

    var i: Int = 1
    var valor: Double? = 0.0
    var contador: Double = 0.0

    while (i != 0) {
        try {
            print("Ingrese un numero: ")
            valor = readLine()?.toDouble()!!
            sumaParcial += valor
            contador ++
        } catch (e: NumberFormatException) {
            println("Esperaba un entero, me diste otra cosa")
            promedio_total = (sumaParcial / contador)
            i = 0
        } catch (e: NullPointerException) {
            println("Esperaba un entero, me diste null, ni siquiera un String")
            promedio_total = (sumaParcial / contador)
            i = 0 //sale del while
        }
    }

    return promedio_total
}