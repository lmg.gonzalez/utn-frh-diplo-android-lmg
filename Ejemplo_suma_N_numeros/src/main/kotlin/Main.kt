fun main() {

    var suma_total: Int? = suma2()
    println("La suma total es = $suma_total")

}

fun suma2(): Int? {

    var cant: String?
    var sumaParcial: Int? = 0

    print("Cuantos numeros vas a querer sumar?")
    cant = readLine()

    var n2: Int? = cant?.toInt()

    if (n2 != null ) {

        var i: Int = 0
        var valor: String?

        while (i < n2) {

            print("Ingrese un numero: ")
            valor = readLine()
            var n: Int? = valor?.toInt()
            if (n != null && sumaParcial != null) {
                sumaParcial += n
            }
            i++
        }
    }
    return sumaParcial
}