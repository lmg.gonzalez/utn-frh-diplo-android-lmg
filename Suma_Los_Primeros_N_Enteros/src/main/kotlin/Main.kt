fun main() {

    var sumaTotal: Int? = sumatoria()
    println("La suma total es = $sumaTotal")

}

fun sumatoria(): Int? {

    var i: Int = 0
    var sumaParcial: Int? = 0
    var valor: String?

    print("Desde el 0, ¿Cuantos numeros correlativos quiere sumar?: ")
    valor = readLine()
    var n: Int = valor!!.toInt()

    while (i <= n) {

        if (n != null && sumaParcial != null){
            sumaParcial += i
        }

        i ++
    }

    return sumaParcial
}